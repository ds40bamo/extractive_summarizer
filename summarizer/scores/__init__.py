import pandas as pd
from pandas.core.dtypes.common import is_period_dtype
import spacy
import langdetect

from summarizer.util import Tokenizer

from .tfidf import tfidf_score
from .position import position_score
from .average_lexical_connectivity import average_lexical_connectivity
from .stopword_ratio import stopword_ratio
from .word_overlap import WordOverlap
from .length import length_score
from .rank import rank_score

MODELS = {"en": "en_core_web_sm", "de": "de_core_news_sm"}


def load_model(model):
    try:
        return spacy.load(model)
    except OSError:
        print(f"model {model} not present")
        print(f"downloading model {model}")
        spacy.cli.download(model)
    return spacy.load(model)


class Scorer:
    def __init__(self, lang="en", rank_score_limit=3, raise_invalid_lang=True):
        self.lang = lang.lower()
        self.rank_score_limit = rank_score_limit
        self.raise_invalid_lang = raise_invalid_lang
        model = MODELS[self.lang]
        nlp = load_model(model)
        self.tokenizer = Tokenizer(nlp)
        self.nlp = nlp

    def get_features(self, document, title=None):
        if self.raise_invalid_lang:
            detected_lang = langdetect.detect(document)
            if detected_lang != self.lang:
                raise ValueError(f"the language of the text ({detected_lang}) and of the loaded model ({self.lang}) do not match")
        sentences = self.tokenizer.tokenize(document)
        if not sentences:
            return []
        scores = pd.DataFrame()
        scores["tfidf"] = tfidf_score(sentences, use_lemma=True, sum=False)
        scores["position"] = position_score(sentences, linear=False, use_exp=True)
        scores["average_lexical_connectivity"] = average_lexical_connectivity(sentences)
        scores["stopword_ratio"] = stopword_ratio(sentences)
        scores["length"] = length_score(sentences)
        if title is not None:
            scores["word_overlap"] = WordOverlap(title, nlp=self.nlp).score(sentences)
        scores["rank"] = rank_score(
            sentences, scores.prod(axis=1), limit=self.rank_score_limit
        )
        sentences = [sent.text.strip() for sent in sentences]
        return sentences, scores

    def score(self, document, title=None):
        sentences, scores = self.get_features(document, title)
        scores = scores.prod(axis=1).values
        return sentences, scores

    def summarize(self, document, title=None, num_sentences=3):
        sentences, scores = self.score(document, title)
        df = pd.DataFrame({"sentences": sentences, "scores": scores})
        df = df.reset_index()
        df = df.sort_values("scores", ascending=False)
        df = df[:num_sentences]
        df = df.sort_values("index")
        summary = " ".join(df["sentences"])
        return summary

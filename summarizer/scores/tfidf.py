# TODO: length penalty
# TODO: try normalized tfidf (length normalized)
from sklearn.feature_extraction.text import TfidfVectorizer
from functools import partial
import spacy
import numpy as np
from summarizer.util import filter_tokens


def sum_feature(matrix):
    """add up the entries of the matrix for each row"""
    return np.asarray(matrix.sum(axis=1)).ravel()


def mul_feature(matrix):
    """add 1 to each entry in the matrix and multiply
    the entries for each row"""
    features = []
    for row_index in range(matrix.shape[0]):
        feature = (matrix[row_index].data + 1).prod()
        features.append(feature)
    return np.array(features)


def tfidf_score(sentences, sum=True, smooth_idf=False, use_lemma=False):
    """compute the tfidf vectors and add the entries up by
    summation or multiplication after adding 1
    """
    analyzer = partial(filter_tokens, use_lemma=use_lemma)
    vectorizer = TfidfVectorizer(analyzer=analyzer, smooth_idf=smooth_idf)
    try:
        tfidf = vectorizer.fit_transform(sentences)
    except ValueError:
        return np.ones(len(sentences))
    features = sum_feature(tfidf) if sum else mul_feature(tfidf)
    return features

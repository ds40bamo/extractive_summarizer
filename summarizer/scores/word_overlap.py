import spacy
import numpy as np
from summarizer.util import Tokenizer, filter_tokens


class WordOverlap:
    def __init__(self, words, nlp):
        if isinstance(words, str):
            words = filter_tokens(Tokenizer(nlp).tokenize(words), use_lemma=True)
        else:
            words = words.copy()
        self.words = set(words)

    def score(self, sentences, log_smooth=True):
        """number of words shared with the title"""
        features = []
        for sentence in sentences:
            words = set(filter_tokens(sentence, use_lemma=True))
            feature = len(words & self.words)
            features.append(feature)
        features = np.array(features)
        if log_smooth:
            features = np.log(features + 1) + 1
        else:
            features += 1
        return features

import spacy
from newspaper import Article


def download_article(url):
    article = Article(url)
    article.download()
    article.parse()
    return {"text": article.text, "title": article.title}


class Tokenizer:
    def __init__(self, nlp):
        self.nlp = nlp

    def tokenize(self, document):
        return [sent for sent in self.nlp(document).sents if sent.text.strip() != ""]


def filter_tokens(tokens, remove_stopwords=True, use_lemma=False):
    return [
        token.lemma_.lower() if use_lemma else token.text
        for token in tokens
        if (not remove_stopwords or not token.is_stop)
        and not token.is_punct
        and not token.is_space
    ]
